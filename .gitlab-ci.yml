image: python:3.6
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache"

include:
  - "/docker/build-ci.yml"

stages:
  - lint
  - test
  - pages
  - build_base
  - build
  - publish
  - review
  # - production

cache:
  paths:
    - .cache/pip
    - venv/

lint:
  stage: lint
  image:
    name: registry.gitlab.com/meltano/meltano/base
    entrypoint: [""]
  before_script:
    - pip install black
  script:
    - make show_lint

test:
  stage: test
  image:
    name: registry.gitlab.com/meltano/meltano/base
    entrypoint: [""]
  variables:
    POSTGRES_ADDRESS: postgres
    POSTGRES_USER: runner
    POSTGRES_DB: pytest
    POSTGRES_PASSWORD: ""
    POSTGRES_PORT: 5432
    # these are the Meltano specific config
    PG_ADDRESS: $POSTGRES_ADDRESS
    PG_USERNAME: $POSTGRES_USER
    PG_PASSWORD: $POSTGRES_PASSWORD
    PG_DATABASE: $POSTGRES_DB
    PG_PORT: $POSTGRES_PORT
  services:
    - postgres:latest
  before_script:
    - make bundle
    - pip install '.[dev]'
  script:
    - pytest -v

package:
  stage: test
  image:
    name: registry.gitlab.com/meltano/meltano/base
    entrypoint: [""]
  script:
    - make sdist
  artifacts:
    paths:
      - dist

pages:
  stage: pages
  image: node:9.11.1
  cache:
    paths:
      - node_modules/
  script:
    - yarn
    - yarn build:docs
    - mv docs/public public
  artifacts:
    paths:
      - public
  only:
    - master

publish:
  image:
    name: registry.gitlab.com/meltano/meltano/base
    entrypoint: [""]
  stage: publish
  before_script:
    - pip install twine
  script:
    - make sdist
    - twine upload dist/*
  only:
    variables:
      - $CI_COMMIT_TAG


.review:
  image: silvs/kubectl:latest
  stage: review
  script:
    - echo ""
  only:
    - branches
  except:
    - master


#############
# Publish   #
#############

# registry.gitlab.com/meltano/meltano:<tag> → docker.io/meltano:<tag>
# registry.gitlab.com/meltano/meltano:latest → docker.io/meltano:latest
hub_meltano: &docker_hub_publish
  image: docker:latest
  stage: publish
  services:
    - docker:dind
  variables:
    DOCKERFILE: .
    DOCKER_DRIVER: overlay2
    IMAGE_NAME: meltano/meltano
    IMAGE_TAG: $CI_COMMIT_TAG
    SOURCE_IMAGE: $CI_REGISTRY_IMAGE
  script:
    - docker pull $SOURCE_IMAGE:$IMAGE_TAG
    - docker pull $SOURCE_IMAGE:latest
    - docker login -u $DOCKER_HUB_USER -p $DOCKER_HUB_PASSWORD
    - docker tag $SOURCE_IMAGE:$IMAGE_TAG $IMAGE_NAME:$IMAGE_TAG
    - docker tag $SOURCE_IMAGE:latest $IMAGE_NAME:latest
    - docker push $IMAGE_NAME:$IMAGE_TAG
    - docker push $IMAGE_NAME:latest
  only:
    variables:
      - $CI_COMMIT_TAG

# registry.gitlab.com/meltano/meltano/cli:<tag> → docker.io/meltano/cli:<tag>
# registry.gitlab.com/meltano/meltano/cli:latest → docker.io/meltano/cli:latest
hub_meltano_cli:
  <<: *docker_hub_publish
  variables:
    IMAGE_NAME: meltano/cli
    IMAGE_TAG: $CI_COMMIT_TAG
    SOURCE_IMAGE: $CI_REGISTRY_IMAGE/cli

.production:
  image: silvs/kubectl:latest
  stage: production
  environment:
    name: production
    url: https://melt.gitlab-bizops.com
  script:
    - cd app/chart/meltano
    - helm init --client-only
    - helm dep update
    - helm upgrade --install --namespace default meltano . --set image.repository=$CI_REGISTRY_IMAGE/meltano,image.tag=base-$CI_COMMIT_SHA,ingress.enabled=true,'global.ingress.hosts[0]=melt.gitlab-bizops.com','global.ingress.tls[0].hosts[0]=melt.gitlab-bizops.com','global.ingress.tls[0].secretName=melt-tls',postgresql.postgresPassword=$POSTGRES_PASSWORD,oauth2-proxy.clientId=$OAUTH2_PROXY_CLIENT_ID,oauth2-proxy.clientSecret=$OAUTH2_PROXY_CLIENT_SECRET,oauth2-proxy.cookieSecret=$OAUTH2_PROXY_COOKIE_SECRET,oauth2-proxy.enabled=true,global.oauth2-proxy.enabled=true,oauth2-proxy.emailDomain=gitlab.com,oauth2-proxy.gitlabHost=https://dev.gitlab.org
  only:
    - master
